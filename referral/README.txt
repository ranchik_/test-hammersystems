# Проект "refApp" API

Данный проект представляет собой API для управления приглашениями и активацией кодов приглашений.

## Установка

1. Склонируйте репозиторий на свой компьютер.
2. Создайте виртуальное окружение: `python -m venv venv`
3. Активируйте виртуальное окружение:
   - На Windows: `venv\Scripts\activate`
   - На macOS и Linux: `source venv/bin/activate`
4. Установите зависимости: `pip install -r requirements.txt`
5. Создайте и примените миграции: `python manage.py makemigrations` и `python manage.py migrate`
6. Запустите сервер разработки: `python manage.py runserver`

## Эндпоинты

### Создание кода авторизации

URL: `/main/`

Метод: `POST`

Запрос:
```json
{
    "phone_number": "+1234567890"
}

Ответ:

{
    "auth_code": "1234"
}

Активация кода приглашения
URL: /profile/

Метод: POST

Запрос:

{
    "activated_invite_code": "IC8map"
}
Ответ:

{
    "phone_number": "+1234567890",
    "invite_code": "bfCrtv",
    "activated_invite_code": "IC8map"
}

Получение профиля пользователя
URL: /api/profile/

Метод: GET

Ответ:

{
    "phone_number": "+1234567890",
    "invite_code": "bfCrtv",
    "activated_invite_code": "IC8map"
    "used_by": []
}

Документация API ReDoc:
http://127.0.0.1:8000/swagger/