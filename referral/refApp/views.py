import time
import random

from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Person, InviteCode
from .serializers import InviteCodeSerializer, PersonSerializer
from .utils import generate_invite_code


class AuthAPIView(APIView):
    def get(self, request):
        return Response(status=status.HTTP_200_OK)

    def post(self, request):
        if 'phone_number' in request.data:
            phone_number = request.data['phone_number']
            auth_code = str(random.randint(1000, 9999))
            #print(auth_code)
            # Здесь может быть логика отправки кода на номер телефона
            time.sleep(1)  # Задержка на сервере 1 секунда

            # Сохранение номера телефона и кода авторизации в сессии
            request.session['phone_number'] = phone_number
            request.session['auth_code'] = auth_code

            return Response({'auth_code': auth_code}, status=status.HTTP_200_OK)

        elif 'auth_code' in request.data:
            auth_code = request.data['auth_code']
            expected_code = request.session.get('auth_code')
            if auth_code == expected_code:
                phone_number = request.session.get('phone_number')
                person, created = Person.objects.get_or_create(phone_number=phone_number)
                if created:
                    invite_code = generate_invite_code()
                    invite = InviteCode.objects.create(code=invite_code, user=person)
                    person.invite_code = invite
                person.save()
                return Response(status=status.HTTP_302_FOUND, headers={'Location': '/profile/'})
            else:
                return Response({'error': 'Неверный код авторизации'}, status=status.HTTP_400_BAD_REQUEST)


class ProfileAPIView(APIView):
    serializer_class = PersonSerializer

    def get(self, request):
        phone_number = request.session.get('phone_number')
        person = Person.objects.get(phone_number=phone_number)
        serializer = self.serializer_class(person)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request):
        phone_number = request.session.get('phone_number')
        person = Person.objects.get(phone_number=phone_number)

        if person.activated_invite_code:
            serializer = self.serializer_class(person)
            return Response(serializer.data, status=status.HTTP_200_OK)

        activated_invite_code = request.data.get('activated_invite_code')

        if activated_invite_code:
            try:
                invite = InviteCode.objects.get(code=activated_invite_code)
            except InviteCode.DoesNotExist:
                return Response({'error': 'Неверный инвайт-код'}, status=status.HTTP_400_BAD_REQUEST)

            person.activated_invite_code = activated_invite_code
            person.save()

            invite.used_by.add(person)
            invite.save()

            serializer = self.serializer_class(person)
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response({'error': 'Введите активирующий код'}, status=status.HTTP_400_BAD_REQUEST)
