from django.contrib.auth.base_user import AbstractBaseUser
from django.db import models


class Person(models.Model):
    phone_number = models.CharField(max_length=22, unique=True, verbose_name="Номер телефона")
    invite_code = models.OneToOneField('InviteCode', on_delete=models.SET_NULL, null=True, blank=True, verbose_name="Ваш код приглашения")
    activated_invite_code = models.CharField(max_length=6, null=True, blank=True, verbose_name="Активированный код приглашения")

    def __str__(self):
        return f"Пользователь - '{self.phone_number}'"

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'


class InviteCode(models.Model):
    code = models.CharField(max_length=6, unique=True, verbose_name="Код приглашения")
    user = models.OneToOneField(Person, on_delete=models.CASCADE, related_name='invite_code_owner', verbose_name="Пользователь")
    used_by = models.ManyToManyField(Person, related_name='used_invite_codes', blank=True, verbose_name="Использовали код:")

    def __str__(self):
        return f"Код - '{self.code}'"

    class Meta:
        verbose_name = 'Код приглашения'
        verbose_name_plural = 'Коды приглашения'

