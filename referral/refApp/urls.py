from django.conf import settings
from django.urls import path

from .views import AuthAPIView, ProfileAPIView


urlpatterns = [
    path('main/', AuthAPIView.as_view(), name='main'),
    path('profile/', ProfileAPIView.as_view(), name='profile'),
]