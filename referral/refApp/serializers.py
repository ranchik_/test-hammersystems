from rest_framework import serializers
from .models import Person, InviteCode


class InviteCodeSerializer(serializers.ModelSerializer):
    class Meta:
        model = InviteCode
        fields = '__all__'


class PersonSerializer(serializers.ModelSerializer):
    invite_code = serializers.SerializerMethodField()
    used_by = serializers.SerializerMethodField()

    def get_invite_code(self, obj):
        if obj.invite_code:
            return obj.invite_code.code
        return None

    def get_used_by(self, obj):
        if obj.invite_code and obj.invite_code.user == obj:
            used_by_users = obj.invite_code.used_by.all()
            used_by_phones = [user.phone_number for user in used_by_users]
            return used_by_phones
        return []

    class Meta:
        model = Person
        fields = ['phone_number', 'invite_code', 'activated_invite_code', 'used_by']

