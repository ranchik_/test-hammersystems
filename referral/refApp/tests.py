from rest_framework.test import APITestCase
from rest_framework import status
from django.urls import reverse

from .models import Person


class AuthAPIViewTest(APITestCase):
    def test_auth_request(self):
        url = reverse('main')
        response = self.client.post(url, data={'phone_number': '+1234567890'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn('auth_code', response.data)


class ProfileAPIViewTest(APITestCase):
    def test_profile_request(self):
        # Создаем пользователя для теста
        person = Person.objects.create(phone_number='+1234567890')

        # Выполняем запрос аутентификации
        auth_url = reverse('main')
        auth_response = self.client.post(auth_url, data={'phone_number': '+1234567890'})
        auth_code = auth_response.data['auth_code']

        # Выполняем запрос профиля с использованием аутентификации
        profile_url = reverse('profile')
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {auth_code}')
        profile_response = self.client.get(profile_url)  # Заменено на GET, так как в view используется GET
        self.assertEqual(profile_response.status_code, status.HTTP_200_OK)